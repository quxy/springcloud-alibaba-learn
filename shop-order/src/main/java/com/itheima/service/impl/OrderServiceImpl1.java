package com.itheima.service.impl;

import com.alibaba.fastjson.JSON;
import com.itheima.domain.Order;
import com.itheima.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * UserServiceImpl
 *
 * @author jjh
 * @date 2020/2/22
 **/
@Slf4j
//@Service
public class OrderServiceImpl1 implements OrderService {

    /**
     * 创建订单
     * @param order
     */
    @Override
    public void create(Order order) {
        order.setOid(1);
        try {
            Thread.sleep(1000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("保存订单信息：{}", JSON.toJSONString(order));
    }
}
