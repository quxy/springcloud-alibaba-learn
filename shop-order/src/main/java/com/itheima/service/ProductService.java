package com.itheima.service;

import com.itheima.domain.Product;
import com.itheima.service.fallback.ProductServiceFallBack;
import com.itheima.service.fallback.ProductServiceFallBackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 商品服务
 * fallback参数指定容错类
 */
@FeignClient(value = "server-product"//,
//        fallback = ProductServiceFallBack.class
//        fallbackFactory = ProductServiceFallBackFactory.class
)
public interface ProductService {

    @GetMapping("/product/{pid}")
    Product findByPid(@PathVariable("pid") Integer pid);

    @RequestMapping("/product/reduce")
    void reduce(@RequestParam("pid") Integer pid,@RequestParam("num") Integer number);
}
