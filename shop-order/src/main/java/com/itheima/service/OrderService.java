package com.itheima.service;

import com.itheima.domain.Order;

/**
 * UserService
 *
 * @author jjh
 * @date 2020/2/22
 **/
public interface OrderService {

    /**
     * 创建订单
     * @param order
     */
    void create(Order order);
}
