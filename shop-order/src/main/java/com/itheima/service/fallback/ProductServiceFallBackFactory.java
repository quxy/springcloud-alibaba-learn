package com.itheima.service.fallback;

import com.itheima.domain.Product;
import com.itheima.service.ProductService;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * （可拿到异常）
 * 这是一个容错类，需要实现feign所在的接口，并实现其中的方法
 * 一旦Feign远程调用异常，就会调用当前类中同名方法
 * @author jjh
 * @date 2020/3/3
 **/
@Slf4j
//@Service
public class ProductServiceFallBackFactory implements FallbackFactory<ProductService> {
    // Throwable 是feign在调用过程中产生的异常
    @Override
    public ProductService create(Throwable throwable) {
        log.error("{}", throwable);

        return new ProductService() {
            @Override
            public Product findByPid(Integer pid) {
                // 容错逻辑
                Product product = new Product();
                product.setPid(-100);
                product.setName("远程调用微服务异常，进入容错处理");
                return product;
            }

            @Override
            public void reduce(Integer pid, Integer number) {

            }
        };
    }
}
