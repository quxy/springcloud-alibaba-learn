package com.itheima.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.fastjson.JSON;
import com.itheima.domain.Order;
import com.itheima.domain.Product;
import com.itheima.exception.BlockException;
import com.itheima.service.OrderService;
import com.itheima.service.ProductService;
import com.itheima.service.impl.OrderServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * UserController
 *
 * @author jjh
 * @date 2020/2/22
 **/
@Slf4j
@RestController
public class OrderController {

    @Autowired
    private RestTemplate restTemplate;

    @Resource
    private RocketMQTemplate rocketMQTemplate;

    @Autowired
    private OrderServiceImpl orderService;

    @Autowired
    private DiscoveryClient discoveryClient;

    int i = 0;

    /**
     * 下单
     * ribbon+feign
     * @return
     */
    @RequestMapping("/order/prod/{pid}")
    public Order order(@PathVariable("pid") Integer pid)  {
        log.info("接收到{}商品的下单请求，接下来调用商品微服务查询商品信息", pid);

        return orderService.create(pid);
    }


}
