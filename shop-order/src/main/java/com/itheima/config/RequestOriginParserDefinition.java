package com.itheima.config;

import com.alibaba.csp.sentinel.adapter.servlet.callback.RequestOriginParser;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * RequestOriginParser
 *
 * @author jjh
 * @date 2020/2/27
 **/
//@Component
public class RequestOriginParserDefinition implements RequestOriginParser {

    /**
     * 定义来源：PC/App
     * 交给流控进行匹配
     *
     * @param httpServletRequest
     * @return
     */
    @Override
    public String parseOrigin(HttpServletRequest httpServletRequest) {
        String serverName = httpServletRequest.getParameter("serverName");
        if (serverName == null) {
            throw new RuntimeException("serverName can`t be bull");
        }
        return serverName;
    }
}
