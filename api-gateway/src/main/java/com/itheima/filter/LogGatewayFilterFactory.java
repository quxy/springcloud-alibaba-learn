package com.itheima.filter;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/**
 * LogGatewayFilterFactory
 *
 * @author jjh
 * @date 2020/3/5
 **/
@Slf4j
@Component
public class LogGatewayFilterFactory extends AbstractGatewayFilterFactory<LogGatewayFilterFactory.Config> {

    public LogGatewayFilterFactory() {
        super(Config.class);
    }

    @Override
    public List<String> shortcutFieldOrder() {
        return Arrays.asList("consoleLog", "cacheLog");
    }

    @Override
    public GatewayFilter apply(Config config) {
        return (exchange, chain) -> {
            if (Boolean.TRUE.equals(config.getCacheLog())) {
                log.info("缓存日志开启。。。");
            }
            if (Boolean.TRUE.equals(config.getConsoleLog())) {
                log.info("控制台日志开启。。。");
            }
            return chain.filter(exchange);
        };
    }

    @Data
    public static class Config{
        private Boolean consoleLog;
        private Boolean cacheLog;
    }
}
