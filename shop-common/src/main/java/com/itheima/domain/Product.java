package com.itheima.domain;

import lombok.Data;

import javax.persistence.*;

/**
 * Product
 *
 * @author jjh
 * @date 2020/2/22
 **/
//@Entity(name = "shop_product")
@Data
@Entity(name = "it_product")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer pid;
    private String name;
    private Double price;
    private Integer stock;
    private String content;

}
