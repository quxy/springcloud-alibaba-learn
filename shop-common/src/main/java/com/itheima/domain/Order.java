package com.itheima.domain;

import lombok.Data;

import javax.persistence.*;

/**
 * Product
 *
 * @author jjh
 * @date 2020/2/22
 **/
//@Entity(name = "shop_product")
@Data
@Entity(name = "it_order")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer oid;
    private Integer pid;
    private Integer uid;
    private Double price;
    private Integer number;
    private String content;

}
