package com.itheima.domain;

import lombok.Data;

import javax.persistence.*;

/**
 * User
 *
 * @author jjh
 * @date 2020/2/22
 **/
//@Entity(name = "shop_user")
@Data
@Entity(name = "it_user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer uid;
    private String username;
    private String password;
    private String telephone;

}
