package com.itheima.service;

import com.itheima.domain.Product;

/**
 * UserService
 *
 * @author jjh
 * @date 2020/2/22
 **/
public interface ProductService {

    /**
     * 查询商品信息
     * @param pid   商品ID
     * @return
     */
    Product findByPid(Integer pid);

    /**
     * 保存商品
     * @param product
     */
    void save(Product product);

    /**
     * 扣库存
     * @param pid 商品ID
     * @param number 减少的库存
     */
    void reduce(Integer pid, Integer number);
}
