package com.itheima.controller;

import com.alibaba.fastjson.JSON;
import com.itheima.domain.Product;
import com.itheima.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * UserController
 *
 * @author jjh
 * @date 2020/2/22
 **/
//@RefreshScope  //只需要在需要动态读取配置的类上添加此注解就可以动态读取配置
@Slf4j
@RestController
public class ProductController {

    @Autowired
    private ProductService productService;

//    @Value("${config.appName}")
//    private String appName;
//
//    @GetMapping("/appName")
//    public String appName() {
//        return appName;
//    }
//
//    @Value("${config.env}")
//    private String env;
//
//    @GetMapping("/env")
//    public String env() {
//        return env;
//    }

    @RequestMapping("/product/{pid}")
    public Product product(@PathVariable("pid") Integer pid) {

        log.info("开始查询 pid={} 的商品信息。", pid);

        Product product = productService.findByPid(pid);

        log.info("查询成功：商品内容：{}", JSON.toJSONString(product));
        return product;
    }

    //减少库存
    @RequestMapping("/product/reduce")
    public void reduceInventory(Integer pid, int num) {
        productService.reduce(pid, num);
    }

    @GetMapping("/product/save")
    public String save(Product product){
        productService.save(product);
        return String.valueOf(product.getPid());
    }

    @RequestMapping("/product/api1/demo1")
    public String demo11(){
        return "api1/demo1";
    }

    @RequestMapping("/product/api1/demo2")
    public String demo12(){
        return "api1/demo2";
    }

    @RequestMapping("/product/api2/demo1")
    public String demo21(){
        return "api2/demo1";
    }

    @RequestMapping("/product/api2/demo2")
    public String demo22(){
        return "api2/demo2";
    }


}
